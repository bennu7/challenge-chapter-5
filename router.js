const express = require("express");
const router = express.Router();

const postComment = require("./controllers/post_comment");
const postLike = require("./controllers/post_like");
const posts = require("./controllers/post");
const user = require("./controllers/user");

// post comment
router.post("/comments", postComment.createPostComment);
router.get("/comments", postComment.getAllPostComment);
router.get("/comments/:id", postComment.getOnePostComment);
router.put("/comments/:id", postComment.updatePostComment);
router.delete("/comments/:id", postComment.deletPostComment);

// post like
router.post("/likes", postLike.createPostLike);
router.get("/likes", postLike.getAllPostLike);
router.get("/likes/:id", postLike.getOnePostLike);
router.put("/likes/:id", postLike.updatePostLike);
router.delete("/likes/:id", postLike.deletePostLike);

// post
router.post("/posts", posts.createPost);
router.get("/posts", posts.getAllPosts);
router.get("/posts/:id", posts.getOnePost);
router.put("/posts/:id", posts.updatePost);
router.delete("/posts/:id", posts.deletePost);

// user
router.post("/users", user.createUser);
router.get("/users", user.getAllUser);
router.get("/users/:id", user.getOneUser);
router.put("/users/:id", user.updateUser);
router.delete("/users/:id", user.deleteUser);

module.exports = router;
