const express = require("express");
const morgan = require("morgan");
const app = express();
const port = 3000;
const router = require("./router");
const swaggerJSON = require("./swagger.json");
const swaggerUI = require("swagger-ui-express");

app.set("view engine", "ejs");

app.use(morgan("dev"));
app.use(express.json());

app.use("/api/v1/", router);
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

// handle error endpoint
app.use((req, res) => {
  res.status(404).render("404");
});

app.listen(port, () => {
  console.log(`server berjalan di http://localhost:${port}/`);
});
